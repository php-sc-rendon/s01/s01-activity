<?php require_once "./code.php"?>
<!--linking code.php to this page -->


<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>
</head>
<body>

	<h3>Letter-Based Grading</h3>
	<p><?php echo getLetterGrade(99) ?></p>
	<p><?php echo getLetterGrade(76) ?></p>
	<p><?php echo getLetterGrade(102) ?></p>

</body>
</html>